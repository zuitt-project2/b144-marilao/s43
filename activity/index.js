let posts = [];
let count = 1;

// innerHTML property refers to the literal HTML markup
// value property refers to the content of typically an HTML input control

// Add post data. Other approach
document.querySelector('#form-add-post').addEventListener('submit', (event) => {
	// Prevents the page from loading
	event.preventDefault();

	posts.push({ //to show data that have been inputted after clicking the create button
		id: count,
		title: document.querySelector('#txt-title').value,
		body: document.querySelector('#txt-body').value
	});

	count++; //to continue add movie posts that will be creating/adding in the input.

	showPosts(posts);
	alert('Successfully added.')
})

// Show posts

const showPosts = (posts) => {
	let postEntries = '';

	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>

		`;
	})
	
	document.querySelector('#div-post-entries').innerHTML = postEntries
}

// miniactivity update
/*for (var i = 0; i < posts.length; i++) {
  if (posts[i].Id === showPosts) {
    posts[i].postEntries = "";
    break;
  }
}*/

// Edit post
	const editPost = (id) => {
		let title = document.querySelector(`#post-title-${id}`).innerHTML;
		let body = document.querySelector(`#post-body-${id}`).innerHTML;

		// initialize by adding document.querySelector
		document.querySelector('#txt-edit-id').value = id;
		document.querySelector('#txt-edit-title').value = title;
		document.querySelector('#txt-edit-body').value = body;
		
	}
// Update
	document.querySelector('#form-edit-post').addEventListener('submit' ,(event) => {
		event.preventDefault()


		// for loop is as search engine
		// id = 1 index 0
		for(let i = 0; i < posts.length; i++){

			if(posts[i].id.toString() === document.querySelector('#txt-edit-id').value){
				posts[i].title = document.querySelector('#txt-edit-title').value;
				posts[i].body = document.querySelector('#txt-edit-body').value;

				showPosts(posts);
				alert('Successfully updated.');

				break;
			}
		}
	})


// Delete 

const deletePost = (all) => {
	posts = posts.filter(post => post.id != all );
	alert('Deleted Successfully!')
	showPosts(posts);
}

		

